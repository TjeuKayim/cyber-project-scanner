# Reads targets.txt and removes duplicate ip addresses, so only the ips that were scanned once remain.
fileName = "targets.txt"
ips = []
with open(fileName, "r") as file:
    lines = file.read().splitlines()
    for line in lines:
        if lines.count(line) == 1:
            ips.append(line)
with open(fileName, "w") as file:
    file.write("\n".join(ips))
