# Scripting OpenVAS
Project Vulnerability Scanning Service

## Creating a new task
Start Nmap scan

    ./nmap-scan 192.168.4.0/24

Examine output in `targets.txt`

    cat targets.txt

Create a new target from that list of ip addresses

    ./new-target "Demo target"

Create a task for that target

    ./new-task "Demo target"

Start OpenVAS scan

    ./start "Scan Demo target"